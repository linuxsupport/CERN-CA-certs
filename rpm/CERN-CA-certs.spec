Summary: CERN CA certificates for use with system libraries
Name: @PACKAGE@
Version: @VERSION@
Release: @RELEASE@%{?dist}
URL: http://cern.ch/ca
Packager: Linux.Support@cern.ch
Source0: %{name}-%{version}.tar.gz
License: distributable
Group: System/Security
BuildRoot: %{_tmppath}/%{name}-root
BuildRequires: openssl
Obsoletes: openssl-CERN-CA-certificates openssl-CERN-CA-certificates-slc5 openssl-CERN-CA-certs
BuildArch: noarch
Requires: coreutils
Requires: grep

%description
Certificates of CERN Certificate authority (https://cern.ch/ca)
shipped as .crt .bundle and .pem and also imported to system java
certificate store(s) for all supported java versions. 

Also includes VeriSign Class 3 Secure Server CA - G3 which is missing
in system ca-bundle on SLC5/6.

And in addtion AddTrust External CA Root which is included in the
ca-bundle but owncloud client needs it in hashed form.

Install this if you would like openssl (and other commands that use
openssl libraries) to trust certificates signed by this CA.


%prep
%setup -q

%build

for i in CERN_Root_CA.crt CERN_Root_Certification_Authority_2.crt CERN_Grid_Certification_Authority.crt CERN_Certification_Authority.crt CERN_Certification_Authority\(1\).crt VeriSignClass3SecureServerCA-G3.crt AddTrustExternalCARoot.crt; do

  j=`basename $i .crt`
  openssl x509 -inform DER -in "${i}" -outform PEM -out "${j}.pem"

  num=`cat "${j}.pem" | grep -E 'BEGIN.* CERTIFICATE' | wc -l`
  if [ "$num" -gt 1 ]; then
     echo "Error: more than 1 certificate in $j.pem, can't handle"
     exit 7
  fi

  hash=`openssl x509 -in  $j.pem -noout -hash`
%if 0%{?rhel} >= 6
  oldhash=`openssl x509 -in  $j.pem -noout -subject_hash_old`
%endif
  # is this always .0? or should we have the version here?
  # not until we get a clash, this is a uniqifier.
  ln -sf "${j}.pem" "${hash}.0"
%if 0%{?rhel} >= 6
  ln -sf "${j}.pem" "${oldhash}.0"
%endif
  # subject:
  subject=`openssl x509 -subject -noout -in $j.pem | sed -e 's/subject= *//'`

if [[ "$i" =~ "CERN" ]]; then
  # signing policy
  cat > "${j}.signing_policy" <<EOF
access_id_CA      X509         '$subject'
pos_rights        globus        CA:sign
cond_subjects     globus     '"/DC=ch/DC=cern/*"'
EOF

  ln -sf "${j}.signing_policy" "${hash}.signing_policy"
%if 0%{?rhel} >= 6
  ln -sf "${j}.signing_policy" "${oldhash}.signing_policy"
%endif

fi

  chmod 0444 *
done

cat AddTrustExternalCARoot.crt VeriSignClass3SecureServerCA-G3.crt CERN_Root_CA.crt CERN_Root_Certification_Authority_2.crt CERN_Grid_Certification_Authority.crt CERN_Certification_Authority.crt CERN_Certification_Authority\(1\).crt > CERN-bundle.crt
cat AddTrustExternalCARoot.pem VeriSignClass3SecureServerCA-G3.pem CERN_Root_CA.pem CERN_Root_Certification_Authority_2.pem CERN_Grid_Certification_Authority.pem CERN_Certification_Authority.pem CERN_Certification_Authority\(1\).pem > CERN-bundle.pem


# CRL?

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/pki/tls/{certs,crl}
mkdir -p $RPM_BUILD_ROOT/etc/openldap/cacerts
mkdir -p $RPM_BUILD_ROOT/usr/share/pki/ca-trust-source/anchors

install -m 0444 CERN* $RPM_BUILD_ROOT/etc/pki/tls/certs/
install -m 0444 CERN*.crt $RPM_BUILD_ROOT/usr/share/pki/ca-trust-source/anchors/
install -m 0444 *signing_policy $RPM_BUILD_ROOT/etc/pki/tls/certs/ 
install -m 0444 *0 $RPM_BUILD_ROOT/etc/pki/tls/certs/ 
install -m 0444 VeriSign* $RPM_BUILD_ROOT/etc/pki/tls/certs/
install -m 0444 AddTrustExternalCARoot* $RPM_BUILD_ROOT/etc/pki/tls/certs/

# only *.pem certs in openldap cacerts: no signing policy .. etc
install -m 0444 *.pem $RPM_BUILD_ROOT/etc/openldap/cacerts/
# and hashes: openldap lib uses hashes 
install -m 0444 *0 $RPM_BUILD_ROOT/etc/openldap/cacerts/

mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
install -m 0755 cern-import-certs-java.sh $RPM_BUILD_ROOT/%{_sbindir}/cern-import-certs-java

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/etc/pki/tls/certs/*
/etc/openldap/cacerts/*
/usr/share/pki/ca-trust-source/anchors/CERN*.crt
%{_sbindir}/cern-import-certs-java

%post
%{_sbindir}/cern-import-certs-java || :

if [ -x /usr/bin/update-ca-trust ]; then
  if [ `/usr/bin/update-ca-trust check | /bin/grep -c DISABLED` -eq 1 ]; then
    # we need force to overwrite /etc/pki/java/cacerts on SLC6
    /usr/bin/update-ca-trust force-enable || :
  fi
  /usr/bin/update-ca-trust extract || :
fi

%triggerin -- java-1.6.0-openjdk
%{_sbindir}/cern-import-certs-java || :

%triggerin -- java-1.7.0-openjdk
%{_sbindir}/cern-import-certs-java || :

%triggerin -- java-1.7.0-oracle
%{_sbindir}/cern-import-certs-java || :

%triggerin -- java-1.6.0-sun
%{_sbindir}/cern-import-certs-java || :

%triggerin -- java-1.8.0-oracle
%{_sbindir}/cern-import-certs-java || :

%triggerin -- java-1.8.0-openjdk
%{_sbindir}/cern-import-certs-java || :

%triggerin -- ca-certificates 
%{_sbindir}/cern-import-certs-java || :

%changelog
* Wed May 16 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20180516-1
- add missing CERN_Certification_Authority(1).crt to java cert store.

* Tue Mar 13 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20180313-1
- removed expired CERN_Trusted_Certification_Authority

* Mon Oct 09 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> 2017100901
- added new CERN_Certification_Authority(1).crt/pem

* Thu May 19 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20160421-2
- added missing coreutils, grep rpm dependency

* Thu Apr 21 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20160421-1
- enable consolidated dynamic configuration of CA certificates
  on SLC6 too.

* Tue Jul 21 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20150721-1
- add consolidated and dynamic configuration of CA certificates 
  using update-ca-trust (SLC6, CC7)

* Wed Feb 18 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20150218-1
- adding AddTrust External CA Root for owncloud client

* Fri Oct 24 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20140325-3
- adding triggers for java-1.8.0-{oracle,openjdk}

* Tue Mar 25 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20140325-1
- Add CERN Certification Authority certificate.

* Thu Nov 21 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-4
- silence cern-import-certs-java

* Fri Nov  8 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-3
- added cern-import-certs-java
* Fri Jul  5 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-2
- added cert hashes to /etc/openldap/cacerts/
* Thu Jun 27 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-1
- added certs for new CERN CA ('CERN CA 2')

* Thu Apr  4 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-10
- only *.pem certificates in /etc/openldap/cacerts/ to avoid 
  authconfig errors.
* Tue Nov 27 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-8
- added VeriSign Class 3 Secure Server CA - G3.

* Tue Jul 24 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-7
- added 'old style hashes' (pre opensssl 1.0)

* Fri Jun 22 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120622-1
- Added certificcates for openldap
* Fri Jun 22 2012 Thomas Oulevey <thomas.oulevey@cern.ch>
- Fixes for .0 files than were not included in previous package.
 
* Thu Mar 22 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch>
* Tue Feb 16 2010 Jan van Eldik <Jan.van.Eldik@cern.ch>
- Unify builds on SLC4 and SLC5

* Fri May  4 2007 Jan Iven  <jan.iven@cern.ch> -20061003.1
- Initial build.


